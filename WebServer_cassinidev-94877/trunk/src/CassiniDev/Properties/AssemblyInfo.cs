﻿using System;
using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Permissions;

[assembly: AssemblyTitle("MATP WebServer")]
[assembly: AssemblyDescription("MATP WebServer")]
[assembly: AssemblyCompany("soshare.cn")]
[assembly: AssemblyProduct("MATP WebServer")]
[assembly: AssemblyCopyright("Soshare.cn,Sky Sanders, Microsoft Corporation. All rights reserved.")]
[assembly: NeutralResourcesLanguage("en-US")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: ComVisible(false)]
[assembly: RuntimeCompatibility(WrapNonExceptionThrows = true)]
[assembly: SecurityPermission(SecurityAction.RequestMinimum)]
[assembly: CLSCompliant(true)]
