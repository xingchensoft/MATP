﻿using System;
using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Permissions;

//[assembly: AssemblyTitle("CassiniDev")]
//[assembly: AssemblyDescription("Cassini For Developers")]
//[assembly: AssemblyCompany("Salient Solutions")]
//[assembly: AssemblyProduct("CassiniDev")]
//[assembly: AssemblyCopyright("\x00a9 Sky Sanders/\x00a9 Microsoft Corporation. All rights reserved.")]
//[assembly: NeutralResourcesLanguage("en-US")]
//[assembly: AssemblyVersion("4.0.1.6")]
//[assembly: AssemblyFileVersion("4.0.1.6")]
//[assembly: ComVisible(false)]
//[assembly: RuntimeCompatibility(WrapNonExceptionThrows = true)]
//[assembly: SecurityPermission(SecurityAction.RequestMinimum)]
//[assembly: CLSCompliant(true)]
[assembly: AssemblyTitle("MATP WebServer")]
[assembly: AssemblyDescription("MATP WebServer")]
[assembly: AssemblyCompany("soshare.cn")]
[assembly: AssemblyProduct("MATP WebServer")]
[assembly: AssemblyCopyright("Soshare.cn,Sky Sanders, Microsoft Corporation. All rights reserved.")]
[assembly: NeutralResourcesLanguage("en-US")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: ComVisible(false)]
[assembly: RuntimeCompatibility(WrapNonExceptionThrows = true)]
[assembly: SecurityPermission(SecurityAction.RequestMinimum)]
[assembly: CLSCompliant(true)]